<x-html-head></x-html-head>

<body>

    <script src="https://cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.js"></script>


    <div id="app">
        <v-app>
            <v-data-table class="scroll-y elevation-1" :dense="dense" :dark="dark" height="94vh" :headers="headers"
                :single-expand="singleExpand" :items="failedResults" sort-desc sort-by="lastLogin" disable-pagination
                hide-default-footer fixed-header :expanded.sync="expanded" :search="search" :loading="loading"
                show-select show-expand :single-select="singleSelect" v-model="selected"
                @click:row="(item, slot) => rowClick(item, slot)" item-key="ID">
                <template v-slot:top v-slot:activator="{ on }">
                    <v-toolbar flat dark id="scroll-target">
                        <v-img src="ad_logo_white.png" max-height="50" max-width="100"></v-img>
                        <v-divider class="mx-4" inset vertical></v-divider>
                        <v-toolbar-title>Salesforce racuni</v-toolbar-title>

                        <v-divider inset vertical class="ml-5 mr-3"></v-divider>
                        <v-card-title>
                            <v-text-field :value="search" @change="(v) => (search = v)" append-icon="mdi-magnify"
                                label="Pretraga po imenu" hide-details>
                            </v-text-field>
                        </v-card-title>
                        <input class="mr-1" type="checkbox" v-model="dark" v-on:change="darkDenseChanged" />
                        <label>Mračni način</label>

                        <v-spacer></v-spacer>
                        <v-divider inset vertical class="ml-2 mr-2"></v-divider>
                    </v-toolbar>
                </template>

                <template v-slot:expanded-item="{ headers, item }">
                    <td :colspan="headers.length">@{{ item . TransferLog }}</td>
                </template>

                <template v-slot:item.actions="{ item }">
                    <v-icon small class="mr-2" @click="pushOne(item)">
                        mdi-forward
                    </v-icon>
                </template>
                <template v-slot:no-data>
                    <v-btn color="primary" @click="initialize"> Reset </v-btn>
                </template>
            </v-data-table>
        </v-app>

    </div>
    <script src="dataTable2.js"></script>


</body>
