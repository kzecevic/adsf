<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Env;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;

class InsertController extends Controller
{

    public static function InsertData($token, $file, $object, $externalId = '', $upsert = false)
    {
        if ($upsert) {
            $reqData = [
                'object' => $object,
                'contentType' => "CSV",
                "externalIdFieldName" => $externalId,
                'operation' => "upsert"
            ];
        } else {
            $reqData = [
                'object' => $object,
                'contentType' => "CSV",
                'operation' => "insert"
            ];
        }
        //open ingest job and get jobId
        $response = Http::withToken($token)->withHeaders(['Content-Type: application/json; charset=UTF-8', 'Accept: application/json'])
            ->post('https://aurodomus2-dev-ed.my.salesforce.com/services/data/v53.0/jobs/ingest', $reqData);
        error_log('after open job: ' . $response);

        if ($response['state'] == "Open") {

            $jobId = $response['id'];

            //ingest csv file
            $response = Http::withToken($token)->accept('application/json')
                ->withBody(file_get_contents($file), 'text/csv')
                ->put(
                    'https://aurodomus2-dev-ed.my.salesforce.com/services/data/v53.0/jobs/ingest/' . $jobId . '/batches/'
                );
            error_log('after batch upload: ' . $response);

            //send upload complete and trigger batch job validation
            $response = Http::withToken($token)->withHeaders(['Content-Type: application/json; charset=UTF-8', 'Accept: application/json'])
                ->patch('https://aurodomus2-dev-ed.my.salesforce.com/services/data/v53.0/jobs/ingest/' . $jobId . '/', [
                    'state' => "UploadComplete"
                ]);
            error_log('after upload complete sent: ' . $response);

            while (true) {
                //get state of batch job
                $response = Http::withToken($token)->accept('application/json')
                    ->get('https://aurodomus2-dev-ed.my.salesforce.com/services/data/v53.0/jobs/ingest/' . $jobId);
                error_log('after get state: ' . $response);

                if ($response['state'] != 'InProgress' && $response['state'] != "UploadComplete") { //if done processing

                    //get failed/success results
                    $response = Http::withToken($token)->accept('text/csv')
                        ->get('https://aurodomus2-dev-ed.my.salesforce.com/services/data/v53.0/jobs/ingest/' . $jobId . '/failedResults');
                    error_log('failed/successful results: ' . $response);

                    break;
                }

                usleep(300 * 1000); //300ms
            }
        }
        return $response;
    }

    public function InsertBulk(Request $req)
    {
        try {
            //get Oauth2 token
            $token = Http::asForm()->post(env('SALESFORCE_URL').'/services/oauth2/token', [
                'client_id' => '3MVG9t0sl2P.pByoUxTUrcmPuOYtzyD6GOxPlVe457onlj0DIItLwgrOwx5ZLZaF_sYjwohS5jPKFd3dQM7aW',
                'client_secret' => '847B6BA2E65579995C9BE92D2393D32C44E67F11C7206D70D30AFAD312ED4C41',
                'username' => env('SALESFORCE_USERNAME'),
                'password' => env('SALESFORCE_PASSWORD'),
                'grant_type' => 'password'
            ])['access_token'];



            //get "distinct" users from database"
            $users = DB::select('select Otkup2011.dbo.maknibljuzge_Kruno(ime+prezime+Adresa+Mjesto) as id, ime, prezime, adresa,mjesto
                from
                (select Otkup2011.dbo.maknibljuzge_Kruno(ime+prezime+Adresa+Mjesto) as id , ime,prezime, adresa,mjesto,ROW_NUMBER()
                over (PARTITION by Otkup2011.dbo.maknibljuzge_Kruno(ime+prezime+Adresa+Mjesto) order by ime) as rn
                from auroinvest.dbo.TempRacunik) tmp
                where rn = 1');

            //prepare csv data and headers for accounts insert
            $file = fopen('accountsInvest.csv', 'w');
            fputcsv($file, [
                'ExtId__c',
                'Name',
                'RecordTypeId'
            ]);
            foreach ($users as $key => $value) {
                $value->ime = str_replace("\n", '', $value->ime);
                $value->prezime = str_replace("\n", '', $value->prezime);
                $value->ime = str_replace(" ", '', $value->ime);
                $value->prezime = str_replace(" ", '', $value->prezime);

                if (($value->ime == null || $value->ime == '') && ($value->prezime == null || $value->prezime == '')) {
                    $value->ime = 'Nema';
                    $value->prezime = 'Podataka';
                    $value->id = 'NemaPodataka';
                }
                $value->mjesto = str_replace("\n", '', $value->mjesto);
                $value->adresa = str_replace("\n", '', $value->adresa);
                fputcsv($file, [
                    $value->id,
                    $value->ime . ' ' . $value->prezime,
                    '0127Q000000TpCwQAK'
                ]);
            }
            fclose($file);
            InsertController::InsertData($token, 'accountsInvest.csv', 'Account', 'ExtId__c', true); //inserts accounts

            //prepare csv for contacts insert
            $file = fopen('ContactsInvest.csv', 'w');
            fputcsv($file, [
                'Account.ExtId__c',
                'ExtId__c',
                'FirstName',
                'LastName'
            ]);
            foreach ($users as $key => $value) {
                fputcsv($file, [
                    $value->id,
                    $value->id,
                    $value->ime,
                    $value->prezime
                ]);
            }
            fclose($file);
            $response = InsertController::InsertData($token, 'ContactsInvest.csv', 'Contact', 'ExtId__c', true); //inserts

            $cases = DB::select('select id_racunik, kasa,CONVERT(varchar,nadnevak,126) as nadnevak, operateri.Imeprezime as operater, iznossapdv,Otkup2011.dbo.maknibljuzge_Kruno(racunik.ime+racunik.prezime+racunik.Adresa+racunik.Mjesto) as klijent
                                    from auroinvest.dbo.tempracunik racunik
                                    join auroinvest.dbo.operateri on operateri.id_operateri = racunik.operater');

            //prepare csv for "case" insert
            $file = fopen('CaseInvest.csv', 'w');
            fputcsv($file, [
                'Contact.ExtId__c',
                'ExtId__c',
                'Kasa__c',
                'Nadnevak__c',
                'Operater__c',
                'Iznossapdv__c'
            ]);

            foreach ($cases as $key => $value) {

                if ($value->klijent == null || $value->klijent == null) {
                    $value->klijent = "NemaPodataka";
                }
                fputcsv($file, [
                    $value->klijent,
                    $value->id_racunik,
                    $value->kasa,
                    $value->nadnevak,
                    $value->operater,
                    $value->iznossapdv
                ]);
            }
            fclose($file);
            $response = InsertController::InsertData($token, 'CaseInvest.csv', 'Case', 'ExtId__c', true); //inserts


            return response($response);
        } catch (\Exception $e) {
            // if ($file) {
            //     fclose($file);
            // }
            throw $e;
        }
    }
}
