<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FailedResultsController extends Controller
{
    function GetFailedUsers()
    {
        try {
            return response(DB::select('select * from development.dbo.SF_UserId where Transfer = 0 or Transfer is null'));

        } catch (\Exception $e) {
            throw $e;
        }
    }

    function GetFailedCases()
    {
        try {
            return response(DB::select('select * from development.dbo.SF_UserId_Rac where Transfer = 0 or Transfer is null'));

        } catch (\Exception $e) {
            throw $e;
        }
    }
}
