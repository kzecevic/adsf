<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\InsertController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\FailedResultsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/insert', [InsertController::class, 'Insert']);
Route::get('/klijenti', function(){
    return view('components.salefsorce-transfer-ui');
});
Route::get('/racuni', function(){
    return view('components.salesforce-transfer-ui-racuni');
});

Route::get('/getFailedUsers', [FailedResultsController::class, 'GetFailedUsers']);
Route::get('/getFailedCases', [FailedResultsController::class, 'GetFailedCases']);

Route::get('/otkup', [InsertController::class, 'InsertOtkup']);
Route::get('/insertbulk', [InsertController::class, 'InsertBulk']);


Route::get('/insertsingleuser', [AccountController::class, 'InsertSingle']);
